package com.practice.tasks.management.repository;

import com.practice.tasks.management.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
