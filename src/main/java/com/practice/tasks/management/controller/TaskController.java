package com.practice.tasks.management.controller;

import com.practice.tasks.management.entity.Task;
import com.practice.tasks.management.exception.ResourceNotFoundException;
import com.practice.tasks.management.service.TaskService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/tasks")
public class TaskController {

    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    @ApiOperation(value = "Get all tasks")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create task")
    public Task create(
            @ApiParam(value = "Task object store in database table", required = true) @RequestBody Task task) {
        return taskService.save(task);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get task by id")
    public Task findById(
            @ApiParam(value = "Task id from which Task object will retrieve", required = true) @PathVariable Long id) throws ResourceNotFoundException {
        return getTaskIfTaskExists(id);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update task")
    public Task update(
            @ApiParam(value = "Task Id to update task object", required = true) @PathVariable Long id,
            @ApiParam(value = "Update task object", required = true) @RequestBody Task taskDetails) throws ResourceNotFoundException {
        Task task = getTaskIfTaskExists(id);

        task.setName(taskDetails.getName());
        task.setGroupName(taskDetails.getGroupName());
        task.setAssignee(taskDetails.getAssignee());
        task.setStartTime(taskDetails.getStartTime());
        task.setEndTime(taskDetails.getEndTime());
        task.setFinished(taskDetails.isFinished());

        return taskService.update(task);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete task")
    public void delete(
            @ApiParam(value = "Task Id from which task object will delete from database table", required = true) @PathVariable Long id) throws ResourceNotFoundException {
        taskService.deleteById(getTaskIfTaskExists(id).getId());
    }

    private Task getTaskIfTaskExists(Long id) throws ResourceNotFoundException {
        return taskService.findById(id).orElseThrow(() -> new ResourceNotFoundException("Task not found for this id :: " + id));
    }
}