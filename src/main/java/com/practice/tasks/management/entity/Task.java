package com.practice.tasks.management.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "tasks")
@ApiModel(description = "All details about the Task. ")
public class Task {

    @GeneratedValue
    @Id
    @ApiModelProperty(notes = "The database generated employee ID")
    private Long id;

    @Column(nullable = false)
    @ApiModelProperty(notes = "The task name")
    private String name;

    @ApiModelProperty(notes = "The task start time")
    private LocalDateTime startTime;

    @ApiModelProperty(notes = "The task end time")
    private LocalDateTime endTime;

    @Column(nullable = false)
    @ApiModelProperty(notes = "The task group name")
    private String groupName;

    @ApiModelProperty(notes = "The task assignee")
    private String assignee;

    @ApiModelProperty(notes = "Is task finished flag")
    @Column(nullable = false)
    private boolean isFinished;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(updatable = false)
    private Task task;

    @OneToMany(mappedBy = "task", orphanRemoval = true)
    @ApiModelProperty(notes = "Sub-tasks")
    private List<Task> subTaskList;

    public Task(Long id, String name, LocalDateTime startTime, LocalDateTime endTime, String groupName, String assignee, boolean isFinished, Task task, List<Task> subTaskList) {
        this.id = id;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.groupName = groupName;
        this.assignee = assignee;
        this.isFinished = isFinished;
        this.task = task;
        this.subTaskList = subTaskList;
    }

    public Task() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public List<Task> getSubTaskList() {
        return subTaskList;
    }

    public void setSubTaskList(List<Task> subTaskList) {
        this.subTaskList = subTaskList;
    }

    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
