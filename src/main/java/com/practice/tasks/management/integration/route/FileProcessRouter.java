package com.practice.tasks.management.integration.route;

import com.practice.tasks.management.entity.Task;
import com.practice.tasks.management.integration.process.TaskUploadProcess;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FileProcessRouter extends RouteBuilder {

    Logger logger = LoggerFactory.getLogger(FileProcessRouter.class);

    @Value("${input.folder.path}")
    private String inputFolder;
    @Value("${output.folder.path}")
    private String outputFolder;
    @Value("${error.folder.path}")
    private String errorFolder;
    private TaskUploadProcess taskUploadProcess;

    public FileProcessRouter(TaskUploadProcess taskUploadProcess) {
        this.taskUploadProcess = taskUploadProcess;
    }

    @Override
    public void configure() {
        JacksonDataFormat jacksonDataFormat = new ListJacksonDataFormat(Task.class);
        from("file:" + inputFolder)
                .doTry().unmarshal(jacksonDataFormat).process(taskUploadProcess).marshal(jacksonDataFormat)
                .to("file:" + outputFolder)
                .doCatch(Throwable.class).process(exchange -> {
            Throwable error = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);
            logger.error("FATAL ERROR - ", error);
        })
                .to("file:" + errorFolder);
    }
}
