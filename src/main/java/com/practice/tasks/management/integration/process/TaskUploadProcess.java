package com.practice.tasks.management.integration.process;

import com.practice.tasks.management.entity.Task;
import com.practice.tasks.management.service.TaskService;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TaskUploadProcess implements Processor {

    private TaskService taskService;

    public TaskUploadProcess(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void process(Exchange exchange) {
        ((List<Task>) exchange.getIn().getBody()).forEach(task -> taskService.save(task));
    }
}
