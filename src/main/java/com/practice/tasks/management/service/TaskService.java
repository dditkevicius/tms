package com.practice.tasks.management.service;

import com.practice.tasks.management.entity.Task;
import com.practice.tasks.management.repository.TaskRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Optional<Task> findById(Long id) {
        return taskRepository.findById(id);
    }

    public Task save(Task task) {
        validate(task);
        Task savedTask = taskRepository.save(task);
        if (task.getSubTaskList() != null) {
            task.getSubTaskList().forEach(t -> {
                t.setTask(savedTask);
                taskRepository.save(t);
            });
        }

        return savedTask;
    }

    public Task update(Task task) {
        validate(task);
        return taskRepository.save(task);
    }

    public void deleteById(Long id) {
        taskRepository.deleteById(id);
    }

    private void validate(Task task) {
        if (task.isFinished() && task.getTask() == null && task.getSubTaskList() != null) {
            task.getSubTaskList().forEach(t -> {
                if (!t.isFinished()) {
                    throw new ResponseStatusException(
                            HttpStatus.BAD_REQUEST, "The task could be finished only if sub-tasks are also finished");
                }
            });
        }
    }

}
