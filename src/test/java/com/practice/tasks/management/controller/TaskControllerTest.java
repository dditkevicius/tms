package com.practice.tasks.management.controller;

import com.practice.tasks.management.entity.Task;
import com.practice.tasks.management.service.TaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(TaskController.class)
public class TaskControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TaskService taskService;

    private Task mockTaskId1 = new Task(1L, "name", null, null, "groupName", "assignee", false, null, null);
    private Task mockTaskId2 = new Task(2L, "name", null, null, "groupName", "assignee", false, null, null);
    private String jsonContent1 = "{id:1,name:name,startTime:null,endTime:null,groupName:groupName,assignee:assignee,subTaskList:null,finished:false}";
    private String jsonContent2 = "{id:2,name:name,startTime:null,endTime:null,groupName:groupName,assignee:assignee,subTaskList:null,finished:false}";

    @Test
    public void findAll_basic() throws Exception {

        when(taskService.findAll())
                .thenReturn(Arrays.asList(mockTaskId1,
                        mockTaskId2));

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/tasks")
                .accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("[" + jsonContent1 + ", " +
                        jsonContent2 + "]"))
                .andReturn();
    }

    @Test
    public void create_basic() throws Exception {

        String exampleTaskJson = "{\"id\":1,\"name\":\"name\",\"startTime\":null,\"endTime\":null,\"groupName\":\"groupName\",\"assignee\":\"assignee\",\"subTaskList\":null,\"finished\":false}";

        when(taskService.save(mockTaskId1))
                .thenReturn(mockTaskId1);

        RequestBuilder request = MockMvcRequestBuilders
                .post("/api/tasks")
                .accept(MediaType.APPLICATION_JSON)
                .content(exampleTaskJson)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(request)
                .andExpect(status().isCreated())
                .andReturn();
    }

    @Test
    public void findById_basic() throws Exception {
        when(taskService.findById(1L)).
                thenReturn(Optional.of(mockTaskId1));

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/tasks/1")
                .accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json(jsonContent1))
                .andReturn();

    }

    @Test
    public void findById_basic_with_resourceNotFoundException() throws Exception {

        RequestBuilder request = MockMvcRequestBuilders
                .get("/api/tasks/1")
                .accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(request)
                .andExpect(status().isNotFound())
                .andReturn();

    }

    // TODO implement other tests

}